import Cookies from 'js-cookie'

export const state = () => ({
    token: null,
    id: null,
  })

export const mutations = {
    SET_TOKEN(state, token) {
        state.token = token
        this.$axios.defaults.headers.common.Authorization = `Bearer ${token}`
    },
    USER_ID(state, id){
        state.user_id = id
        localStorage.setItem('id', JSON.stringify(id))
    },
    REMOVE_TOKEN(state) {
        this.$axios.defaults.headers.common.Authorization = null
        state.token = null
        state.id = null
    },


}
export const actions = {
    setToken({commit}, token) {
        var now = new Date()
        var time = now.getTime()
        var expireTime = time + 60 * 60 * 24 * 7
        const expiryTime = now.setTime(expireTime)

        Cookies.set('access_token', token, {expires: expiryTime})
        commit('SET_TOKEN', token)
    },

    logout({commit}) {
        this.$axios.setToken(false)
        Cookies.remove('access_token')
        localStorage.clear()
        commit('REMOVE_TOKEN')
    },


}
