import Cookies from 'js-cookie'
export default function (context) {
    context.$axios.onRequest(config => {
      if(Cookies.get('access_token')){
        context.$axios.setToken(Cookies.get('access_token'), 'Bearer')
      }
    })

    context.$axios.onError(error => {
        const code = parseInt(error.response && error.response.status)
        if (code === 401) {
            Cookies.remove('access_token')
            context.redirect('/login')
        }
    })
}
